import { ExtendedAccount } from 'dblurt';
import {
  getAccount,
  BlockchainGlobalProps,
  parseToken,
  vestsToRshares,
} from '~/providers/blurt/dblurtApi';

// export const estimateVoteAmount = (
//   account: ExtendedAccount,
//   globalProps: BlockchainGlobalProps, 
//   voteWeight = 1,
//   ) => {
//   let usedMana = (current_mana * (voteWeight * 100) * 60 * 60 * 24) / BLURT_100_PERCENT;
//   const maxVoteDenom = vote_power_reserve_rate * BLURT_VOTING_MANA_REGENERATION_SECONDS;
//   usedMana = (usedMana + maxVoteDenom - 1) / maxVoteDenom;
//   let rshares = parseInt(usedMana);
//   if (cashoutDelta < BLURT_UPVOTE_LOCKOUT_SECONDS) {
//     rshares = parseInt((rshares * cashoutDelta) / BLURT_UPVOTE_LOCKOUT_SECONDS);
//   }
//   const totalRshares = rshares + postRshares;
//   const claims = parseInt((totalRshares * (totalRshares + 2 * S)) / (totalRshares + 4 * S));
//   const rewards = rewardBalance / recentClaims;
//   const postValue = claims * rewards * ratio;
//   return postValue * (rshares / totalRshares);
// }

export const estimateVoteAmount = (
  account: ExtendedAccount,
  globalProps: BlockchainGlobalProps,
  voteWeight = 1,
) => {
  const { fundRecentClaims, fundRewardBalance, base, quote } = globalProps;
  // get account
  const votingPower = account.voting_power;
  const totalVests =
    parseToken(account.vesting_shares as string) +
    parseToken(account.received_vesting_shares as string) -
    parseToken(account.delegated_vesting_shares as string);

  const votePct = voteWeight * 10000;
  const rShares = vestsToRshares(totalVests, votingPower, votePct);
  return (
    ((rShares / fundRecentClaims) * fundRewardBalance) /
    2
  ).toFixed(2);

  const temp1 = totalVests * 1e6;
  const maxAmount =
    (temp1 * fundRewardBalance * ((base * 0.02) / quote)) / fundRecentClaims;
  //  console.log('maxAmount', maxAmount);
  const amount = ((maxAmount * votingPower) / 1e4) * voteWeight;

  return amount.toFixed(2);
};
