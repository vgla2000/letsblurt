const functions = require('firebase-functions');
const axios = require('axios');
const dblurt = require('dblurt');
const admin = require('firebase-admin');

admin.initializeApp();

const MAINNET_OFFICIAL = [
  'https://rpc.blurt.world',
  "https://blurt-rpc.saboin.com",
  "https://rpc.nerdtopia.de",
  "https://rpc.blurtlatam.com/",
  "https://blurt.ecosynthesizer.com/",
  "https://kentzz.blurt.world/",
];
const client = new dblurt.Client(MAINNET_OFFICIAL, {
  timeout: 5000,
  addressPrefix: 'BLT',
  chainId: 'cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f',
});

// proxy for google custom search
exports.searchRequest = functions.https.onCall(async (data, res) => {
  //  console.log('input data', data);
  const { query, startAt = 1, num = 10, sort = '' } = data;

  const key = functions.config().search.key;
  const cx = functions.config().search.cx;
  const search = `https://www.googleapis.com/customsearch/v1?key=${key}&cx=${cx}&q=${query}&num=${num}&start=${startAt}&sort=${sort}`;

  //    const response = await axios.get(search);
  let result = null;
  await axios
    .get(search)
    .then((response) => {
      result = response.data;
      //      console.log('response result', result);
    })
    .catch((error) => console.log('failed to search', error));

  return result;
});

// proxy for getting supported languages for google translation
exports.getTranslationLanguagesRequest = functions.https.onCall(async () => {
  const key = functions.config().translation.key;
  const url = `https://translation.googleapis.com/language/translate/v2/languages?key=${key}`;

  let result = null;
  await axios
    .get(url)
    .then((response) => {
      result = response.data.data.languages;
    })
    .catch((error) => console.log('failed to get supported translate', error));

  return result;
});

// proxy for google translation v3
exports.translationRequest = functions.https.onCall(async (data, context) => {
  //  console.log('input data', data);
  const { text, targetLang, format } = data;

  const options = {
    target: targetLang,
    q: text,
    format,
  };

  const key = functions.config().translation.key;
  const url = `https://translation.googleapis.com/language/translate/v2?key=${key}`;

  let result = null;
  await axios
    .post(url, options)
    .then((response) => {
      result = response.data;
      //      console.log('response result', result);
    })
    .catch((error) => console.log('failed to translate', error));

  return result;
});

// proxy for creating blurt account
exports.createAccountRequest = functions.https.onCall(async (data, context) => {
  const { username, password, creationFee } = data;

  // get creator account
  const creator = functions.config().creator.account;
  const creatorWif = functions.config().creator.wif;
  const welcomeBlurt = functions.config().creator.welcome_blurt;

  // private active key of creator account
  const creatorKey = dblurt.PrivateKey.fromString(creatorWif);
  // create keys
  const ownerKey = dblurt.PrivateKey.fromLogin(username, password, 'owner');
  const activeKey = dblurt.PrivateKey.fromLogin(username, password, 'active');
  const postingKey = dblurt.PrivateKey.fromLogin(username, password, 'posting');
  const memoKey = dblurt.PrivateKey.fromLogin(
    username,
    password,
    'memo',
  ).createPublic(client.addressPrefix);

  const ownerAuth = {
    weight_threshold: 1,
    account_auths: [],
    key_auths: [[ownerKey.createPublic(client.addressPrefix), 1]],
  };
  const activeAuth = {
    weight_threshold: 1,
    account_auths: [],
    key_auths: [[activeKey.createPublic(client.addressPrefix), 1]],
  };
  const postingAuth = {
    weight_threshold: 1,
    account_auths: [],
    key_auths: [[postingKey.createPublic(client.addressPrefix), 1]],
  };

  //// send creation operation
  // operations
  let operations = [];
  //create operation to transmit
  const create_op = [
    'account_create',
    {
      fee: creationFee,
      creator: creator,
      new_account_name: username,
      owner: ownerAuth,
      active: activeAuth,
      posting: postingAuth,
      memo_key: memoKey,
      json_metadata: '',
      extensions: [],
    },
  ];
  console.log(create_op);
  // push the creation operation
  operations.push(create_op);
  try {
    const result = await client.broadcast.sendOperations(
      operations,
      creatorKey,
    );
    console.log('create account, result', result);

    //// if successful, transfer 3 blurt to the account
    if (result) {
      // get privake key from creator active wif
      const privateKey = dblurt.PrivateKey.from(creatorWif);
      // transfer
      if (privateKey) {
        const args = {
          from: creator,
          to: username,
          amount: welcomeBlurt,
          memo: 'Welcome Gift. Enjoy Blurt',
        };
        const resultTransfer = await client.broadcast.transfer(
          args,
          privateKey,
        );
        console.log('transfer result', resultTransfer);
        return resultTransfer;
      }
      return null;
    }
    return null;
  } catch (error) {
    console.log('failed to create account', error);
    return null;
  }
});

//// request to vote
exports.voteRequest = functions.https.onCall(async (data, context) => {
  const TIME_24H_MILLS = 24 * 3600 * 1000;
  const VOTING_DELAY_MILLS = 5.1 * 60 * 1000; // 5.1 minutes delayed
  let votingWeight = 1;
  const votingWeightRef = admin.firestore().collection('settings').doc('voting');
  await votingWeightRef.get().then((doc) => {
    if (doc.exists) {
      votingWeight = doc.data().weight;
      console.log('voting weight', votingWeight);
    }
  })
    .catch((error) => console.log('failed to get voting weight from firestore'));

  // get creator account
  const creator = functions.config().creator.account;
  // need to create new
  const postingWif = functions.config().creator.postingwif;

  //// get username requested the vote
  const { author, permlink } = data;

  //// check if the user is not in the manual voting list, which is stored in firestore
  let skipVoting = false;
  const votingRef = admin.firestore().collection('manual_voting').doc(`${author}`);
  await votingRef.get().then((snapshot) => {
    if (snapshot.exists) {
      console.log('document exits. skip the process');
      skipVoting = true;
    }
  });
  if (skipVoting) return null;

  console.log('the author does not exist in manual voting list');
  // message to return
  let message = null;
  // get user
  const userRef = admin.firestore().doc(`users/${author}`);
  await userRef.get().then((doc) => {
    //console.log('user doc data', doc.data());
    const lastVotingAt = doc.data().lastVotingAt;
    if (!lastVotingAt) {
      console.log('last voting does not exist');
      // vote
      _votePost({
        voter: creator,
        postingWif: postingWif,
        author: author,
        permlink: permlink,
        weight: votingWeight
      });
    } else {
      const currentTime = new Date().getTime();
      console.log('current time', currentTime);
      console.log('last voting time', lastVotingAt.toMillis());
      const timeDiff = currentTime - lastVotingAt.toMillis();
      console.log('time after the last voting in hours', timeDiff / (1000 * 3600));
      if (timeDiff < TIME_24H_MILLS - 1000 * 3600) {
        // next voting after this time (hours)
        const nextVoting = ((TIME_24H_MILLS - timeDiff) / (1000 * 3600)).toFixed(1);
        console.log('vote by timeout in hours', nextVoting);
        message = `One voting per 24h. Next vote will be after ${nextVoting} hours`;
      } else {
        _votePost({
          voter: creator,
          postingWif: postingWif,
          author: author,
          permlink: permlink,
          weight: votingWeight
        });
      }
    }
  });
  return message;
});

//// send push message to followers when a favorite author creates a new post
exports.pushNewPostRequest = functions.https.onCall(async (data, context) => {
  // get the author
  const { author, permlink } = data;

  //// collect push tokens of followers
  // get all the followers of the author
  const followersRef = admin.firestore().collection('favorites').doc(author).collection('followers');
  // get snapshot of the followers
  const followersSnapshot = await followersRef.get();
  // check the followers
  if (followersSnapshot.length < 1) return;

  // build payload
  const title = 'New post by favorite author';
  const body = `@${author}: ${permlink}`
  const payload = {
    notification: {
      title: title,
      body: body,
    },
    data: {
      title: title,
      body: body,
      operation: 'new_post',
      author: author,
      permlink: permlink,
    },
  };
  console.log('push payload', payload);

  // forEach는 await할 수 없어서 바로 넘어가는 문제 있음.. 해결법???
  // 참고. https://stackoverflow.com/questions/37576685/using-async-await-with-a-foreach-loop
  let userIds = [];
  followersSnapshot.forEach((doc) => {
    userIds.push(doc.id);
  });

  console.log('[pushNewPostRequest] followers Ids', userIds);

  // make promises
  const promises = userIds.map(async (userId) => {
    // get user ref
    const userRef = admin.firestore().collection('users').doc(userId);
    const userSnapshot = await userRef.get();
    if (userSnapshot.data().pushNotifications.includes('new_post')) {
      const pushToken = userSnapshot.data().pushToken;
      console.log('[pushNewPostRequest] user, pushToken', userId, pushToken);
      return admin.messaging().sendToDevice(pushToken, payload, {
        // Required for background/quit data-only messages on iOS
        contentAvailable: true,
        // Required for background/quit data-only messages on Android
        priority: "high",
      });
    }
  });

  const results = await Promise.all(promises);
  console.log('[pushNewPostRequest] promise results', results);
});


/////////////////// helpers ///////////////////
// vote
const _votePost = async ({ voter, postingWif, author, permlink, weight }) => {
  //// vote
  const vote = {
    voter,
    author,
    permlink,
    weight: weight * 100,
  };

  const privateKey = dblurt.PrivateKey.from(postingWif);

  if (privateKey) {
    try {
      const result = await client.broadcast.vote(vote, privateKey);
      if (result) {
        console.log('voted for', author);
        // update the lastVotingAt of the author
        // get user
        const userRef = admin.firestore().doc(`users/${author}`);
        await userRef.get().then((doc) => {
          userRef.update({ lastVotingAt: new Date() });
        });
        return result;
      }
    } catch (error) {
      console.log('failed to vote', error);
    }
    return null;
  }
  console.log('the private key is wrong');
}